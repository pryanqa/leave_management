$(document).ready(function() {

    var store = JSON.parse(localStorage.getItem("location"));
    var refstr = JSON.parse(localStorage.getItem("referstr"));
    $('.username').text(store[refstr.cat][refstr.no].username);
    $('.userid').text(store[refstr.cat][refstr.no].id);
    for (var i = 0; i < store.employee.length; i++) {
        for (var j = 0; j < store.employee[i].leave.length; j++) {
            var lstatus;
            var aprd;
            var dcln;
            if (store.employee[i].leave[j].cancel == "1") {
                aprd = '<p class="text-success">Cancelled</p>';
                dcln = '<p class="text-danger">leave</p>';
            } else if (store.employee[i].leave[j].approval == "1") {
                aprd = '<p class="text-success">Approved!</p>';
                dcln = '<button id="decline" type="button">Decline</button></td>';
            } else if (store.employee[i].leave[j].approval == "0") {
                aprd = '<button  id="approve" class="" type="button">Approve</button>';
                dcln = '<p class="text-danger">Declined</p>';
            } else if (store.employee[i].leave[j].approval == "-") {
                aprd = '<button  id="approve" class="" type="button">Approve</button>';
                dcln = '<button id="decline" type="button">Decline</button></td>';
            }


            $('.applications').append('<tr id="' + i + '' + j + '">' +
                '<td class="' + i + '' + j + '">' + (j + 1) + '</td>' +
                '<td>' + store.employee[i].leave[j].ename + '</td>' +
                '<td>' + store.employee[i].leave[j].empid + '</td>' +
                '<td>' + store.employee[i].leave[j].type + '</td>' +
                '<td>' + store.employee[i].leave[j].from + '</td>' +
                '<td>' + store.employee[i].leave[j].to + '</td>' +
                '<td>' + store.employee[i].leave[j].days + '</td>' +
                '<td>' + store.employee[i].leave[j].msg + '</td>' +
                '<td>' + aprd + '</td>' +
                '<td>' + dcln + '</tr>');
        }
    }
    localStorage.getItem("hjson") == null ? localStorage.setItem("hjson", $("#hr").text()) : "";
    var hrs = JSON.parse(localStorage.getItem("hjson"));
    for (var i = 0; i < hrs.hrdata.length; i++) {
        $('.disp').append('<tr>' +
            '<td>' + hrs.hrdata[i].date + '</td>' +
            '<td>' + hrs.hrdata[i].impact + '</td>' +
            '</tr>');
    }
    $(document).on("click", ".add", function() {
        $('#error').text("");
        if ($('#imp').val() == "") {
            $('#error').text(" date required");
            return false;
        }
        var today = new Date();
        var thisdate = new Date($('#imp').val());
        var gap = thisdate.getDate() - today.getDate();
        var mgap = thisdate.getMonth() - today.getMonth();
        var yr = thisdate.getFullYear() - today.getFullYear();
        gap = gap + mgap * 30 + 365 * yr;
        if (gap < 0) {
            $('#error').text("invalid event date");
            return false;
        }
        hrs.hrdata.push({
            "date": $('#imp').val(),
            "impact": $('#impact').val()
        })
        localStorage.setItem("hjson", JSON.stringify(hrs));
        hrs = JSON.parse(localStorage.getItem("hjson"));
        $('.disp').empty();
        for (var i = 0; i < hrs.hrdata.length; i++) {
            $('.disp').append('<tr>' +
                '<td>' + hrs.hrdata[i].date + '</td>' +
                '<td>' + hrs.hrdata[i].impact + '</td>' +
                '</tr>');
        }
    });
    $(document).on("click", "#approve", function() {
        debugger;
        var temp1 = $(this).parents("tr").attr("id");
        var temp2 = $('#' + temp1).children().attr("class");
        for (var i = 0; i < store.employee.length; i++) {
            for (var j = 0; j < store.employee[i].leave.length; j++) {
                if (temp2 == "" + i + "" + j) {
                    if ((store.employee[i].leave[j].approval == "0") || store.employee[i].leave[j].approval == "-") {
                        store.employee[i].leave[j].approval = "1";
                        store.employee[i].leaves = parseInt(store.employee[i].leaves) + parseInt(store.employee[i].leave[j].days);
                    }
                    localStorage.setItem("location", JSON.stringify(store));
                    store = JSON.parse(localStorage.getItem("location"));
                    console.log(store.employee[i].leave[j].approval);
                    location.reload();
                    return false;
                }
            }
        }

    });
    $(document).on("click", "#decline", function() {
        var temp1 = $(this).parents("tr").attr("id");
        var temp2 = $('#' + temp1).children().attr("class");
        for (var i = 0; i < store.employee.length; i++) {
            for (var j = 0; j < store.employee[i].leave.length; j++) {
                if (temp2 == "" + i + "" + j) {
                    if (store.employee[i].leave[j].approval == "1") {
                        store.employee[i].leave[j].approval = "0";
                        store.employee[i].leaves = parseInt(store.employee[i].leaves) - parseInt(store.employee[i].leave[j].days);
                    }
                    if (store.employee[i].leave[j].approval == "-") {
                        store.employee[i].leave[j].approval = "0";
                    }
                    localStorage.setItem("location", JSON.stringify(store));
                    store = JSON.parse(localStorage.getItem("location"));
                    console.log(store.employee[i].leave[j].approval);
                    location.reload();
                    return false;
                }
            }
        }
    });
});